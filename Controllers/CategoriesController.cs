using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Supermarket.Domain.Models;
using Supermarket.Domain.Services;
using Supermarket.Resources;
using Supermarket.Extensions;

namespace supermarket.Controllers
{
  [Route("/api/[controller]")]
  public class CategoriesController : Controller
  {

    // private readonly ILogger<CategoriesController> _logger;
    private readonly ICategoryService _categoryService;
    private readonly IMapper _mapper;

    public CategoriesController(
      ICategoryService categoryService,
      // ILogger<CategoriesController> logger,
      IMapper mapper
    )
    {
      _categoryService = categoryService;
      _mapper = mapper;
      // _logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<CategoryResource>> GetAllAsync()
    {
      var categories = await _categoryService.ListAsync();
      var resources = _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryResource>>(categories);
      return resources;
    }

    [HttpPost]
    public async Task<IActionResult> PostAsync([FromBody] SaveCategoryResource resource)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState.GetErrorMessages());
      }

      var category = _mapper.Map<SaveCategoryResource, Category>(resource);
      var result = await _categoryService.SaveAsync(category);

      if (!result.Success)
        return BadRequest(result.Message);

      var categoryResource = _mapper.Map<Category, CategoryResource>(result.Entity);
      return Ok(categoryResource);
    }


    [HttpPut("{id}")]
    public async Task<IActionResult> PutAsync(int id, [FromBody] SaveCategoryResource resource)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState.GetErrorMessages());

      var category = _mapper.Map<SaveCategoryResource, Category>(resource);
      var result = await _categoryService.UpdateAsync(id, category);

      if (!result.Success)
        return BadRequest(result.Message);

      var categoryResource = _mapper.Map<Category, CategoryResource>(result.Entity);
      return Ok(categoryResource);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAsync(int id)
    {
      var result = await _categoryService.DeleteAsync(id);

      if (!result.Success)
        return BadRequest(result.Message);

      var categoryResource = _mapper.Map<Category, CategoryResource>(result.Entity);
      return Ok(categoryResource);
    }
  }
}
