using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Supermarket.Domain.Models;
using Supermarket.Domain.Services;
using Supermarket.Resources;
using Supermarket.Extensions;

namespace supermarket.Controllers
{
  [Route("/api/[controller]")]
  public class ProductsController : Controller
  {

    // private readonly ILogger<ProductsController> _logger;
    private readonly IProductService _productService;
    private readonly IMapper _mapper;

    public ProductsController(
      IProductService productService,
      // ILogger<ProductsController> logger,
      IMapper mapper
    )
    {
      _productService = productService;
      _mapper = mapper;
      // _logger = logger;
    }

    [HttpGet]
    public async Task<IEnumerable<ProductResource>> ListAsync()
    {
      var products = await _productService.ListAsync();
      var resources = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductResource>>(products);
      return resources;
    }
  }
}
