using System.Collections.Generic;
using System.ComponentModel;

namespace Supermarket.Domain.Models
{
    [Description("POCO for Category")]
    public class Category: Common
    {
        public IList<Product> Products { get; set; } = new List<Product>();
    }
}
