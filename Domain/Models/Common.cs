using System.Collections.Generic;

namespace Supermarket.Domain.Models
{
    public class Common
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
