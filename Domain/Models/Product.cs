using System.ComponentModel;

namespace Supermarket.Domain.Models
{
    [Description("POCO for Prodcuts")]
    public class Product: Common
    {
        public short QuantityInPackage { get; set; }
        public EUnitOfMeasurement UnitOfMeasurement { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
