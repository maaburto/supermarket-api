using System.Collections.Generic;
using System.Threading.Tasks;
using Supermarket.Domain.Models;
using Supermarket.Domain.Services.Communication;

namespace Supermarket.Domain.Services
{
  public interface ICategoryService
  {
    Task<IEnumerable<Category>> ListAsync();
    Task<EntityResponse<Category>> SaveAsync(Category category);
    Task<EntityResponse<Category>> UpdateAsync(int id, Category category);
    Task<EntityResponse<Category>> DeleteAsync(int id);
  }
}