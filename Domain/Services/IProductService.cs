using System.Collections.Generic;
using System.Threading.Tasks;
using Supermarket.Domain.Models;
using Supermarket.Domain.Services.Communication;

namespace Supermarket.Domain.Services
{
  public interface IProductService
  {
    Task<IEnumerable<Product>> ListAsync();
    // Task<EntityResponse<Product>> SaveAsync(Product product);
    // Task<EntityResponse<Product>> UpdateAsync(int id, Product product);
    // Task<EntityResponse<Product>> DeleteAsync(int id);
  }
}