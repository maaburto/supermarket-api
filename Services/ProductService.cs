using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Supermarket.Domain.Models;
using Supermarket.Domain.Services;
using Supermarket.Domain.Repositories;
using Supermarket.Domain.Services.Communication;

namespace Supermarket.Services
{
  public class ProductService : IProductService
  {
    private readonly IProductRepository _productRepository;
    private readonly IUnitOfWork _unitOfWork;

    public ProductService(
      IProductRepository productRepository,
      IUnitOfWork unitOfWork
    )
    {
      _productRepository = productRepository;
      _unitOfWork = unitOfWork;
    }
    public async Task<IEnumerable<Product>> ListAsync()
    {
      return await _productRepository.ListAsync();
    }
  }
}